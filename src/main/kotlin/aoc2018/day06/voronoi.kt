package marko.aoc2018.day06

import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

data class Point(
   val name: String,
   val x: Int,
   val y: Int
)

typealias PointDistance = Pair<Point, Int>

fun getBoundingBox(points: List<Point>): Pair<Point, Point> =
   points.fold(Pair(infinitelyDistantPoint, Point("boundingBox", 0, 0))) { box, item ->
      Pair(
         Point("upperLeft", min(box.first.x, item.x), min(box.first.y, item.y)),
         Point("lowerRight", max(box.second.x, item.x), max(box.second.y, item.y))
      )
   }

fun manhattanDistance(a: Point, b: Point): Int = abs(a.x - b.x) + abs(a.y - b.y)

fun getClosestPoint(current: Point, points: List<Point>): Point? {
   val oneClosestPoint = points.minBy { manhattanDistance(current, it) }
   if (oneClosestPoint == null) {
      return oneClosestPoint
   }
   val distanceToClosest = manhattanDistance(current, oneClosestPoint)
   val closestPoints = points.filter { manhattanDistance(current, it) == distanceToClosest }

   return if (closestPoints.size > 1) {
      null
   } else {
      closestPoints[0]
   }
}

fun getAllPointDistances(current: Point, points: List<Point>): List<PointDistance> = points.map { PointDistance(it, manhattanDistance(current, it)) }

val infinitelyDistantPoint = Point("infinity", Int.MAX_VALUE, Int.MAX_VALUE)

fun getRejected(space: Array<Array<Point>>, boundingBox: Pair<Point, Point>): List<String> =
   listOf(
      space[boundingBox.first.x].map { it.name },
      IntRange(0, space.size - 1).map { space[it][boundingBox.first.y].name },
      space[boundingBox.second.x].map { it.name },
      IntRange(0, space.size - 1).map { space[it][boundingBox.second.y].name },
      listOf("infinity")
   ).flatten().distinct()

fun main(args: Array<String>) {
   val points = input.split("\n")
      .map { it.split(", ") }
      .withIndex()
      .map { Point(it.index.toString(), it.value[0].toInt(), it.value[1].toInt()) }

   val boundingBox = getBoundingBox(points)

   val space = Array(boundingBox.second.x + 1) { Array(boundingBox.second.y + 1) { infinitelyDistantPoint } }

   for (x in 0 until space.size)
      for (y in 0 until space[x].size) {
         val closestPoint = getClosestPoint(Point("current", x, y), points)
         space[x][y] = closestPoint ?: infinitelyDistantPoint
      }

   val fullSpace = Array(boundingBox.second.x + 1) { Array(boundingBox.second.y + 1) { listOf<PointDistance>() } }
   for (x in 0 until space.size)
      for (y in 0 until space[x].size) {
         val distancesToAllPoints = getAllPointDistances(Point("current", x, y), points)
         fullSpace[x][y] = distancesToAllPoints
      }

   val rejectedPoints = getRejected(space, boundingBox)

   val maxSurface = space
      .flatMap { row -> row.map { it.name } }
      .groupingBy { it }
      .eachCount()
      .filter { !rejectedPoints.contains(it.key) }
      .maxBy { it.value }
      ?.value

   val safeSurface = fullSpace
      .map { it -> it.map { cell -> cell.sumBy { pd -> pd.second } }.filter { it < 10000 } }.map { it.size }.sum()

   println("max surface: $maxSurface")
   println("safe surface: $safeSurface")
}