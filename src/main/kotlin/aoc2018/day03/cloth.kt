package marko.aoc2018.day03

import marko.aoc2018.day03.cloth.input

data class Point(
   val x: Int,
   val y: Int
)

data class Patch(
   val id: Int,
   val startingPoint: Point,
   val sizeX: Int,
   val sizeY: Int
)

fun main(args: Array<String>) {
   val patches = input
      .map {
         Patch(it[0], Point(it[1], it[2]), it[3], it[4])
      }

   val cloth = Array(1000) { Array(1000) { mutableListOf<Int>() } }

   patches.forEach {
      for (row in it.startingPoint.y until it.startingPoint.y + it.sizeY) {
         for (col in it.startingPoint.x until it.startingPoint.x + it.sizeX) {
            cloth[row][col].add(it.id)
         }
      }
   }

   val squaresWithMultiplePatches = cloth.map { row ->
      row.filter { it.size > 1 }.count()
   }.sum()

   println(squaresWithMultiplePatches)

   val patchesOverlappingAnother = cloth.flatMap { row ->
      row.filter { it.size > 1 }.flatten()
   }.toSet()

   val nonOverlappingPatches = patches.map { it.id }.toSet() - patchesOverlappingAnother

   nonOverlappingPatches.forEach { println(it) }
}