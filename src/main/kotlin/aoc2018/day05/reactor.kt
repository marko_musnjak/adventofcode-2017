package marko.aoc2018.day05

import java.lang.System.currentTimeMillis
import kotlin.math.abs

fun polymerFold(input: String, dropChar: Char = '_'): String {
   return input
      .fold("") { acc, char ->
         when {
            char.toUpperCase() == dropChar.toUpperCase() -> acc
            acc.isEmpty() -> char.toString()
            abs(acc.last() - char) == 32 -> acc.dropLast(1)
            else -> acc.plus(char)
         }
      }
}

fun main(args: Array<String>) {

   val startTime = currentTimeMillis()

   val foldedSize = polymerFold(input).length

   val endOneTime = currentTimeMillis()

   val minLength = input.map { it.toUpperCase() }.distinct()
      .map {
         Pair(it, polymerFold(input, it).length)
      }
      .minBy { it.second }
      ?.second ?: -1

   val endTwoTime = currentTimeMillis()

   println("$foldedSize")
   println("$minLength")

   print("Step 1: ${endOneTime - startTime} ms, Step 2: ${endTwoTime - endOneTime}")
}