package marko.aoc2018.day02

/**
 * Analyzes the ID, returns an integer representing for which group to count
 * bit map meanings:
 *  0: do not count
 *  1: at least one character appears exactly two times
 *  2: at least one character appears exactly three times
 * @return bitmap of groups to count for
 */
fun countsForGroup(boxID: String): Int {
   val wantedCharacterCounts = boxID
      .map { Pair(it, 1) }
      .groupBy { it.first }
      .map { it -> Pair(it.key, it.value.map { it.second }.sum()) }
      .filter { it.second == 2 || it.second == 3 }
   val someCharactersAppearTwoTimes = wantedCharacterCounts.any { it.second == 2 }
   val someCharactersAppearThreeTimes = wantedCharacterCounts.any { it.second == 3 }
   return (if (someCharactersAppearTwoTimes) 1 else 0) + (if (someCharactersAppearThreeTimes) 2 else 0)
}

fun getChecksum(groupBitMasks: List<Int>): Int {
   val countMatchTwo = groupBitMasks.filter { it and 1 != 0 }.size
   val countMatchThree = groupBitMasks.filter { it and 2 != 0 }.size

   return countMatchTwo * countMatchThree
}

fun cartesianProduct(elements: List<String>): List<Pair<String, String>> {
   if (elements.isEmpty())
      return emptyList()
   val result = mutableListOf<Pair<String, String>>()
   for ((firstIndex, firstElement) in elements.subList(0, elements.size - 1).iterator().withIndex())
      for (secondElement in elements.subList(firstIndex + 1, elements.size).iterator())
         result.add(Pair(firstElement, secondElement))
   return result
}

fun hammingDistance(a: String, b: String): Int {
   return a.zip(b).count { it.first != it.second }
}

fun cleanDiff(a: String, b: String): String {
   return a
      .zip(b)
      .filter { it.first == it.second }
      .map { it.first }
      .joinToString("")
}

fun main(args: Array<String>) {
   val boxIDs = input.split("\n")
   val groupBitMasks = boxIDs.map { countsForGroup(it) }

   println(getChecksum(groupBitMasks))

   val pairs = cartesianProduct(boxIDs)
   val singleCharacterDiffStrings = pairs.filter { hammingDistance(it.first, it.second) == 1 }

   val cleanedUpStrings = singleCharacterDiffStrings.map { cleanDiff(it.first, it.second) }

   println(cleanedUpStrings)
}