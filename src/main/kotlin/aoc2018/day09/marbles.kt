package marko.aoc2018.day09

const val PLAYERS = 455
const val LAST_MARBLE_VALUE = 7122300

class DoublyLinkedCircularList<T> {
   class Node<T>(val data: T, var prev: Node<T>? = null, var next: Node<T>? = null)

   private var current: Node<T>? = null

   private var size = 0

   fun addAfterCurrent(data: T, stepsAway: Int = 0) {
      // println("prev: ${current?.prev?.data}, Current: ${current?.data}, next: ${current?.next?.data}")
      var newNode: Node<T>
      when (current) {
         null -> {
            newNode = Node(data, null, null)
            newNode?.next = newNode
            newNode?.prev = newNode
         }
         current?.next -> {
            newNode = Node(data, current, current)
            current?.next = newNode
            current?.prev = newNode
         }
         else -> {
            IntRange(1, stepsAway).forEach {
               current = current?.next
            }
            newNode = Node(data, current, current?.next)
            if (current?.prev == current) {
               current?.prev = newNode
            }
            current?.next?.prev = newNode
            current?.next = newNode
         }
      }
      current = newNode
      size++
   }

   fun removeBefore(steps: Int): T? {
      IntRange(1, steps).forEach {
         current = current?.prev
      }
      val toReturn = current?.data
      val newCurrent = current?.next
      current?.prev?.next = newCurrent
      newCurrent?.prev = current?.prev
      current = newCurrent
      return toReturn
   }

   override fun toString(): String {
      val sb = StringBuilder()
      var tempCur = current

      sb.append("..  <-> ${tempCur?.prev?.data} ) <-> ")

      for (i in 1..size) {
         sb.append("${tempCur?.data} <-> ")
         tempCur = tempCur?.next
      }
      sb.append("( ${tempCur?.data} <-> ...")
      return sb.toString()
   }
}

fun main(args: Array<String>) {
   val circle = DoublyLinkedCircularList<Int>()
   circle.addAfterCurrent(0)
   val scores = HashMap<Int, Long>()
   var player = 0
   var marble = 0
   var points: Int
   do {
      player = (player + 1) % PLAYERS
      marble++
      if (marble % 23 == 0) {
         val toRemove = circle.removeBefore(7)
         points = marble + (toRemove ?: 0)
         // println ("Removing marble $marble gives player $player $points points")
         scores[player] = scores.getOrDefault(player, 0) + points
         continue
      }
      circle.addAfterCurrent(marble, 1)
   } while (marble < LAST_MARBLE_VALUE)

   val maxScore = scores.maxBy { it.value }?.value ?: 0

   println("max score: $maxScore")

   println("done")
}