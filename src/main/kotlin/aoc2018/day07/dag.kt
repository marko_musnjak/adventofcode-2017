package marko.aoc2018.day07

import kotlin.math.max
import kotlin.math.min

const val WORKERS = 5
const val BASE_DURATION = 60

tailrec fun executeSequentially(
   previousSteps: List<Char>,
   availableTasks: Set<Char>,
   taskPrerequisites: Map<Char, List<Char>>,
   taskDependents: Map<Char, List<Char>>
): List<Char> {
   if (availableTasks.isEmpty()) return previousSteps
   val nextTask = availableTasks
      .filter { previousSteps.containsAll(taskPrerequisites[it] ?: emptyList()) }
      .sorted()
      .first()
   return executeSequentially(
      previousSteps + nextTask,
      availableTasks - nextTask + (taskDependents[nextTask] ?: emptySet()),
      taskPrerequisites,
      taskDependents
   )
}

fun getTaskDuration(t: Char): Int = t.plus(BASE_DURATION).toInt() - 'A'.toInt() + 1

fun executeInParallel(
   currentSecond: Int,
   completedSteps: List<Char>,
   availableTasks: Set<Char>,
   taskPrerequisites: Map<Char, List<Char>>,
   taskDependents: Map<Char, List<Char>>,
   workerStates: Array<Char>,
   remainingDurations: Array<Int>
): Int {
   // we are now at the start of second "currentSecond"
   // tasks with incoming duration 0 are now complete

   println("start of second $currentSecond: ${workerStates.joinToString()} | ${remainingDurations.joinToString()}")

   // record IDs, set their workers to idle
   val completeTasks = remainingDurations.withIndex().filter { it.value == 0 && workerStates[it.index] != '_' }
   val workerStatesWithFinishedTasks = Array(WORKERS) {
      if (completeTasks.map { w -> w.index }.contains(it)) {
         '_'
      } else {
         workerStates[it]
      }
   }

   // prepare list of complete tasks for next second
   val newCompleted = completedSteps + completeTasks.map { it.index }.map { workerStates[it] }

   // find as many available tasks as we have idle workers
   val tasksToStart = availableTasks
      .filter { !newCompleted.contains(it) } // they are not already done
      .filter { !workerStatesWithFinishedTasks.contains(it) } // they are not in progress
      .filter { newCompleted.containsAll(taskPrerequisites[it] ?: emptyList()) } // but their dependencies are
      .sorted() // priorities!
      .take(min(workerStatesWithFinishedTasks.filter { '_' == it }.size, WORKERS)) // don't overwhelm the workers

   // if all the workers are idle and no new tasks are available, we are done!
   if (workerStatesWithFinishedTasks.filter { '_' == it }.count() == WORKERS && tasksToStart.isEmpty()) {
      return currentSecond
   }

   // assign tasks to workers
   val newAssignments = workerStatesWithFinishedTasks
      .withIndex()
      .filter { '_' == it.value }
      .zip(tasksToStart).map { Pair(it.first.index, it.second) }.toMap()

   val newWorkerStates = Array(WORKERS) {
      newAssignments.getOrDefault(it, workerStatesWithFinishedTasks[it])
   }
   val newDurations = Array(WORKERS) {
      if (newAssignments.containsKey(it)) {
         getTaskDuration(newAssignments[it] ?: '_') - 1
      } else {
         max(0, remainingDurations[it] - 1)
      }
   }

   return executeInParallel(
      currentSecond + 1,
      newCompleted,
      availableTasks,
      taskPrerequisites,
      taskDependents,
      newWorkerStates,
      newDurations
   )
}

fun main(args: Array<String>) {
   val inputDependencies = input.split("\n")
      .map { Pair(it[36].toUpperCase(), it[5].toUpperCase()) }

   val allTasks = inputDependencies.flatMap { listOf(it.first, it.second) }.toSet()

   val taskPrerequisites = inputDependencies
      .groupBy { it.first }
      .mapValues { it.value.map { node -> node.second } }

   val taskFollowers = inputDependencies
      .groupBy { it.second }
      .mapValues { it.value.map { node -> node.first } }

   val startingTasks = allTasks.toSet() - taskPrerequisites.keys

   val stepList = executeSequentially(emptyList(), startingTasks, taskPrerequisites, taskFollowers).joinToString("")

   println("sequential steps: $stepList")

   val parallelDuration = executeInParallel(
      0,
      emptyList(),
      allTasks,
      taskPrerequisites,
      taskFollowers,
      Array(WORKERS) { '_' },
      Array(WORKERS) { 0 }
   )

   println("parallel duration: $parallelDuration")
}