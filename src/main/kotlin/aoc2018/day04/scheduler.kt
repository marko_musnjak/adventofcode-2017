package marko.aoc2018.day04

import java.time.LocalDate

data class GuardShift(
   val date: LocalDate,
   val guardId: Int,
   val sleepPeriods: List<SleepPeriod>,
   val sleepByMinute: Array<Int>
)

// data class SleepPeriod (start and end minute)
data class SleepPeriod(
   val startMinute: Int,
   val endMinute: Int
)

fun main(args: Array<String>) {

   val sortedInput = input.split("\n").sorted()

   val schedule = sortedInput
      .fold(mutableListOf<MutableList<String>>()) { acc, item ->
         if (item.contains("Guard")) {
            acc.add(0, mutableListOf(item))
         } else {
            acc.first().add(item)
         }
         acc
      }
      .flatMap {
         if (it.size < 2)
            emptyList()
         else {
            val guardId = it.first().replace("#", "").split(" ")[3].toInt()
            val date = LocalDate.parse(it[1].substring(1..10))
            val sleepPeriods = it
               .drop(1)
               .chunked(2)
               .map { item ->
                  SleepPeriod(item[0].substring(15, 17).toInt(), item[1].substring(15, 17).toInt())
               }
               .toList()
            val sleepByMinute = sleepPeriods
               .fold(Array(60) { 0 }) { acc, item ->
                  for (minute in item.startMinute until item.endMinute) {
                     acc[minute] = 1
                  }
                  acc
               }
            listOf(GuardShift(date, guardId, sleepPeriods, sleepByMinute))
         }
      }
      .groupBy { it.guardId }

   val highestSleeper = schedule
      .map {
         Pair(it.key, it.value.map { i -> i.sleepByMinute.sum() }.sum())
      }
      .sortedBy { it.second }
      .reversed()
      .first()
      .first

   val mostSleptMinute = (schedule[highestSleeper] ?: emptyList())
      .map { it.sleepByMinute }
      .fold(Array(60) { 0 }) { acc, item ->
         acc.zip(item) { a, b -> a + b }.toTypedArray()
      }
      .withIndex()
      .fold(IndexedValue(0, 0)) { max, item ->
         if (item.value > max.value)
            item
         else
            max
      }
      .index

   println("highest sleeper: $highestSleeper, most slept minute: $mostSleptMinute, product: ${highestSleeper * mostSleptMinute}")

   val peakSleepingMinute = schedule
      .map {
         val summedSleepPeriods = it.value
            .map { i -> i.sleepByMinute }
            .fold(Array(60) { 0 }) { acc, item ->
               acc.zip(item) { a, b -> a + b }.toTypedArray()
            }
            .withIndex()
            .maxBy { i -> i.value } ?: IndexedValue(0, 0)
         Pair(it.key, summedSleepPeriods)
      }
      .maxBy { it.second.value }

   val guard = peakSleepingMinute?.first ?: 0
   val minute = peakSleepingMinute?.second?.index ?: 0

   println("Guard: $guard, minute: $minute, product: ${guard * minute}")
}