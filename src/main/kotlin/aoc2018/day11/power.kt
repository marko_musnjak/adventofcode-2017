package marko.aoc2018.day11

import kotlin.math.min

const val SIDE = 300
const val INPUT = 9995 // (8, 9995, ... )

data class Point(
   val X: Int,
   val Y: Int // Neg : Up
)

fun power(x: Int, y: Int): Int {
   val rackID = x + 10
   val step1 = ((rackID * y) + INPUT) * rackID
   return ((step1 - (step1 % 100)) / 100) % 10 - 5
}

fun seqToCoord(seq: Int): Point = Point(seq / SIDE + 1, seq % SIDE + 1)
fun coordToSeq(p: Point): Int = (p.X - 1) * SIDE + p.Y - 1

fun getMaxSquare(x: Int, y: Int): Int = min(SIDE + 1 - x, SIDE + 1 - y)

fun getSquarePower(p: Point, size: Int, powers: Array<Int>): Int =
   IntRange(p.X, p.X + size - 1)
      .map { x ->
         IntRange(p.Y, p.Y + size - 1)
            .map { y ->
               powers[coordToSeq(Point(x, y))]
            }
            .sum()
      }
      .sum()

fun getMaxSquareSizePower(upperLeft: Point, maxSquareSize: Int, powers: Array<Int>): Pair<Int, Int> {
   if (upperLeft.Y == 1) println("Geting all squares at row ${upperLeft.X}, ${upperLeft.Y}, size $maxSquareSize")
   return IntRange(1, maxSquareSize)
      .map { Pair(it, getSquarePower(upperLeft, it, powers)) }
      .maxBy { it.second } ?: Pair(-1, -1)
}

fun main(args: Array<String>) {

   val powers = Array(SIDE * SIDE) { val a = seqToCoord(it); power(a.X, a.Y) }

   val mostPowerfulThreeSquare = IntRange(1, 298)
      .map { x ->
         IntRange(1, 298).map { y ->
            Triple(x, y, getSquarePower(Point(x, y), 3, powers))
         }
      }
      .flatten()
      .maxBy { it.third }

   println(mostPowerfulThreeSquare)

//   val mostPowerfulSquare = IntRange(1, 300)
//      .map { x ->
//         IntRange(1, 300).map { y ->
//            val max = getMaxSquareSizePower(Point(x, y), getMaxSquare(x, y), powers)
//            Triple(Point(x, y), max.first, max.second)
//         }
//      }
//      .flatten()
//      .maxBy { it.third }
//      ?: Triple(Point(-1, -1), -1, -1)
//
//   println("Largest square has top left corner at (${mostPowerfulSquare.first.X}, ${mostPowerfulSquare.first.Y}), size ${mostPowerfulSquare.second}, total power: ${mostPowerfulSquare.third}")

   println("done")
}