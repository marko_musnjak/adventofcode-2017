package marko.aoc2018.day10

import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

data class Point(
   val X: Long, // Neg : Left
   val Y: Long // Neg : Up
)

typealias Vector = Point

data class BoundingBox(
   val minX: Long,
   val maxX: Long,
   val minY: Long,
   val maxY: Long
)

fun positionAtTime(p: Point, v: Vector, t: Long): Point = Point(p.X + v.X * t, p.Y + v.Y * t)

fun boundingBox(points: Collection<Point>): BoundingBox =
   points.fold(BoundingBox(Long.MAX_VALUE, Long.MIN_VALUE, Long.MAX_VALUE, Long.MIN_VALUE)) { bb, p ->
      BoundingBox(
         min(bb.minX, p.X),
         max(bb.maxX, p.X),
         min(bb.minY, p.Y),
         max(bb.maxY, p.Y)
      )
   }

fun boundingBoxSurface(bb: BoundingBox): Long = abs(bb.maxX - bb.minX) * abs(bb.maxY - bb.minY)

fun bbSurfaceAtTime(t: Long, vectors: Collection<Pair<Point, Vector>>): Long {
   val pointsAtTime = vectors.map { positionAtTime(it.first, it.second, t) }
   val boundingBox = boundingBox(pointsAtTime)
   return boundingBoxSurface(boundingBox)
}

tailrec fun gradientSearchForMinimum(
   vectors: Collection<Pair<Point, Vector>>,
   minTime: Long,
   maxTime: Long,
   valueFunction: (t: Long, v: Collection<Pair<Point, Vector>>) -> Long
): Long {
   println("Searching from $minTime to $maxTime")
   val width = maxTime - minTime
   if (width < 100) {
      println("running linear search")
      return LongRange(minTime, maxTime)
         .map { Pair(it, valueFunction(it, vectors)) }
         .minBy { it.second }
         ?.first ?: -1
   }
   val midPoint = minTime + width / 2
   val leftTime = midPoint - 20
   val rightTime = midPoint + 20
   return when {
      valueFunction(leftTime, vectors) > valueFunction(rightTime, vectors) -> gradientSearchForMinimum(vectors, midPoint, maxTime, valueFunction)
      valueFunction(rightTime, vectors) > valueFunction(leftTime, vectors) -> gradientSearchForMinimum(vectors, minTime, midPoint, valueFunction)
      else -> gradientSearchForMinimum(vectors, minTime - 20, maxTime - 20, valueFunction)
   }
}

fun main(args: Array<String>) {
   val vectors = input
      .map {
         Pair(
            Point(
               it.subSequence(10, 16).toString().trim().toLong(),
               it.subSequence(17, 24).toString().trim().toLong()
            ),
            Vector(
               it.subSequence(36, 38).toString().trim().toLong(),
               it.subSequence(39, 42).toString().trim().toLong()
            )
         )
      }

   val minSurfaceTime = gradientSearchForMinimum(vectors, 0, 99999, ::bbSurfaceAtTime)

   val pointsAtMin = vectors
      .map { positionAtTime(it.first, it.second, minSurfaceTime) }
      .groupBy { it.Y }
      .map { Pair(it.key, it.value.sortedBy { i -> i.X }) }
      .sortedBy { it.first }

   val xRange = pointsAtMin
      .map { row -> row.second.minBy { it.X }?.X ?: Long.MAX_VALUE }
      .min() ?: 0

   pointsAtMin
      .map {
         it.second.fold(Pair(StringBuilder(), xRange - 3)) { acc, point ->
            // string builder and last point we've seen

            val sb = acc.first
            for (i in acc.second until point.X - 1) sb.append(' ')
            if (point.X > acc.second) sb.append('#')
            Pair(acc.first, point.X)
         }
      }
      .map { it.first.toString() }
      .forEach { println(it) }

   println("Seen at time $minSurfaceTime")
}
