package marko.aoc2018.day08

data class Node(
   val children: List<Node>,
   val metadata: List<Int>
)

fun parse(numbers: Iterator<Int>): Node {
   val childNodeCount = numbers.next()
   val metadataCount = numbers.next()
   val children = IntRange(1, childNodeCount).map { parse(numbers) }
   val metadata = IntRange(1, metadataCount).map { numbers.next() }
   return Node(children, metadata)
}

fun sumMetadata(node: Node): Int {
   return node.children.map { sumMetadata(it) }.sum() + node.metadata.sum()
}

fun getNodeValue(node: Node): Int {
   if (node.children.isEmpty())
      return node.metadata.sum()
   val wantedNodes = node.metadata
      .map { node.children.getOrElse(it - 1) { Node(emptyList(), emptyList()) } }

   return wantedNodes.map { getNodeValue(it) }
      .sum()
}

fun main(args: Array<String>) {
   val rawData = input.split(" ").map { it.toInt() }.iterator()

   val tree = parse(rawData)

   val metadataSum = sumMetadata(tree)

   println("Metadata sum: $metadataSum")

   val treeValue = getNodeValue(tree)

   println("Tree value: $treeValue")
}