package marko.aoc2018.day01

fun main(args: Array<String>) {
   val movements = input
      .replace("\n", "")
      .split(",")
      .map { it.toInt() }
   val sum = movements.sum()
   val seenFrequencies = HashSet<Int>()
   var currentFrequency = 0
   seenFrequencies.add(currentFrequency)
   var currentPosition = 0
   while (true) {
      val change = movements[currentPosition % movements.size]
      val newFrequency = currentFrequency + change
      currentFrequency = newFrequency
      println("""Old frequency: $currentFrequency, delta: $change, new frequency: $newFrequency""")
      if (seenFrequencies.contains(newFrequency)) {
         println("""Seeing $newFrequency again""")
         break
      } else {
         println("""Seeing $newFrequency for the first time""")
         seenFrequencies.add(newFrequency)
      }
      currentPosition++
   }
   println(sum)
   println(currentFrequency)
}