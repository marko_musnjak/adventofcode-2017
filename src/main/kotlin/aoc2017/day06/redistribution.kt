package aoc2017.day06

fun redistributeOnce(state: List<Int>): List<Int> {
    val max = state.max() ?: state[0]
    // To do this, it removes all of the blocks from the selected bank, then moves to the next (by index) memory bank and inserts one of the blocks.

    val firstMaxPos = state.indexOfFirst { it == max }
    val newState = state.toIntArray()

    newState[firstMaxPos] = 0

    for (pos in firstMaxPos + 1..firstMaxPos + max) {
        newState[(pos % state.size)] += 1
    }
    return newState.toList()
}

fun stepsBeforeRepeat(state: List<Int>): Pair<Int, List<Int>> {
    val seenStates = mutableSetOf<List<Int>>()
    seenStates.add(state)

    var currentState = state
    var newState: List<Int>

    while (true) {
        newState = redistributeOnce(currentState)
        if (seenStates.contains(newState)) {
            break
        }
        seenStates.add(newState)
        currentState = newState
    }
    return Pair(seenStates.size, newState)
}

fun main(args: Array<String>) {
    val input = listOf(0, 5, 10, 0, 11, 14, 13, 4, 11, 8, 8, 7, 1, 4, 12, 11)
    val output = stepsBeforeRepeat(input)
    println(output.first)
    println(stepsBeforeRepeat(output.second).first)
}