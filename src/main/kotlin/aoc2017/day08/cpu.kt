package marko.aoc2017.day08

enum class RELATION(val shortHand: String) {
    EQ("=="),
    LT("<"),
    LEQT("<="),
    GT(">"),
    GEQT(">="),
    NEQ("!=");

    companion object {
        fun fromString(text: String): RELATION {
            for (r in RELATION.values()) {
                if (r.shortHand.equals(text, true)) {
                    return r
                }
            }
            throw IllegalArgumentException("No relation with shorthand $text")
        }
    }
}

enum class OPERATION(val shortHand: String) {
    INC("inc"),
    DEC("dec");

    companion object {
        fun fromString(text: String): OPERATION {
            for (o in OPERATION.values()) {
                if (o.shortHand.equals(text, true)) {
                    return o
                }
            }
            throw IllegalArgumentException("No operation with shorthand $text")
        }
    }
}

data class ConditionalInstruction(
        val register: String,
        val op: OPERATION,
        val amount: Int,
        val conditionRegister: String,
        val relation: RELATION,
        val limit: Int)

data class RegisterWithMax(
        val currentValue: Int,
        val maxValue: Int
)

val lineRegex = """(\w+) (\w+) (-?\d+) if (\w+) (=|!=|>|<|>=|<=|==) (-?\d+)""".toRegex()

fun parseLine(inputLine: String): ConditionalInstruction {
    val matchResult = lineRegex.find(inputLine)
    return ConditionalInstruction(
            matchResult!!.groups[1]!!.value,
            OPERATION.fromString(matchResult.groups[2]!!.value.toUpperCase()),
            matchResult.groups[3]!!.value.toInt(),
            matchResult.groups[4]!!.value,
            RELATION.fromString(matchResult.groups[5]!!.value),
            matchResult.groups[6]!!.value.toInt()
    )
}

fun checkCondition(value: Int, rel: RELATION, limit: Int): Boolean {
    return when (rel) {
        RELATION.EQ -> value == limit
        RELATION.GEQT -> value >= limit
        RELATION.GT -> value > limit
        RELATION.LEQT -> value <= limit
        RELATION.LT -> value < limit
        RELATION.NEQ -> value != limit
    }
}

fun executeOperation(op: OPERATION, registerStates: MutableMap<String, RegisterWithMax>, register: String, amount: Int): Unit {
    val oldState = registerStates.getValue(register)
    val newValue = when (op) {
        OPERATION.INC -> oldState.currentValue + amount
        OPERATION.DEC -> oldState.currentValue - amount
    }
    val newMax = if (newValue > oldState.maxValue) newValue else oldState.maxValue
    registerStates[register] = RegisterWithMax(newValue, newMax)
}

fun processInstruction(instruction: ConditionalInstruction, registerStates: MutableMap<String, RegisterWithMax>): Unit {
    if (checkCondition(registerStates.getValue(instruction.conditionRegister).currentValue, instruction.relation, instruction.limit)) {
        executeOperation(instruction.op, registerStates, instruction.register, instruction.amount)
    }
}

fun loadInstructions(input: String): List<ConditionalInstruction> {
    return input.split("\n").map { parseLine(it) }
}

fun initializeRegisters(instructions: List<ConditionalInstruction>): MutableMap<String, RegisterWithMax> {
    return instructions
            .flatMap { listOf(it.register, it.conditionRegister) }
            .distinct()
            .map { Pair(it, RegisterWithMax(0, 0)) }
            .toMap()
            .toMutableMap()
}

fun main(args: Array<String>) {
    val instructions = loadInstructions(input)
    val registerStates = initializeRegisters(instructions)
    instructions.forEach { processInstruction(it, registerStates) }
    println(registerStates.map { it.value.currentValue }.max())
    println(registerStates.map { it.value.maxValue }.max())
}