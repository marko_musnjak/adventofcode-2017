package aoc2017.day03

import kotlin.math.*

fun getXCoordinateRecursive(num: Int): Int {
    // From the online encyclopedia of integer sequences, A174344
    // a(1) = 0, a(n) = a(n-1) + sin(mod(floor(sqrt(4*(n-2)+1)),4)*Pi/2).
    return if (num == 1) {
        0
    } else {
        getXCoordinateRecursive(num - 1) + sin((floor(sqrt(4 * (num - 2).toDouble() + 1)).toInt() % 4) * PI / 2).roundToInt()
    }
}

fun getXCoordinate(num: Int): Int {
    // From the online encyclopedia of integer sequences, A174344
    // a(1) = 0, a(n) = a(n-1) + sin(mod(floor(sqrt(4*(n-2)+1)),4)*Pi/2).
    var currentX = 0
    for (currentNum in 2..num) {
        currentX += sin((floor(sqrt(4 * (currentNum - 2).toDouble() + 1)).toInt() % 4) * PI / 2).roundToInt()
    }
    return currentX
}

fun getYCoordinateRecursive(num: Int): Int {
    // From the online encyclopedia of integer sequences, A274923
    // a(1) = 0, a(n) = a(n-1) - cos(mod(floor(sqrt(4*(n-2)+1)),4)*Pi/2).
    return if (num == 1) {
        0
    } else {
        getYCoordinate(num - 1) - cos((floor(sqrt(4 * (num - 2).toDouble() + 1)).toInt() % 4) * PI / 2).roundToInt()
    }
}

fun getYCoordinate(num: Int): Int {
    // From the online encyclopedia of integer sequences, A274923
    // a(1) = 0, a(n) = a(n-1) - cos(mod(floor(sqrt(4*(n-2)+1)),4)*Pi/2).
    var currentY = 0
    for (currentNum in 2..num) {
        currentY -= cos((floor(sqrt(4 * (currentNum - 2).toDouble() + 1)).toInt() % 4) * PI / 2).roundToInt()
    }
    return currentY
}

fun manhattanDistanceInSpiral(num: Int): Int {
    return abs(getXCoordinate(num)) + abs(getYCoordinate(num))
}

fun main(args: Array<String>) {
    val input = 289326
    println(manhattanDistanceInSpiral(input))
    // Part 2 is sequence A141481 in OEIS
    // Answer for part 2 is listed in https://oeis.org/A141481/b141481.txt (295229)
}