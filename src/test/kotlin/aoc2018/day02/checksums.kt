package aoc2018.day02

import marko.aoc2018.day02.cartesianProduct
import marko.aoc2018.day02.countsForGroup
import marko.aoc2018.day02.hammingDistance
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ChecksumTest {

    @Test
    fun testSampleCases() {
        assertEquals(0, countsForGroup("abcdef"))
        assertEquals(3, countsForGroup("bababc"))
        assertEquals(1, countsForGroup("abbcde"))
        assertEquals(2, countsForGroup("abcccd"))
        assertEquals(1, countsForGroup("aabcdd"))
        assertEquals(1, countsForGroup("abcdee"))
        assertEquals(2, countsForGroup("ababab"))
    }

    @Test
    fun testEmptyCartesianProduct() {
        assertEquals(emptyList<String>(), cartesianProduct(emptyList()))
    }

    @Test
    fun testSingleElementCartesianProduct() {
        // What in the world should this return?
        // assertEquals(listOf(Pair("a","a")), cartesianProduct(listOf("a")))
        assertEquals(emptyList<String>(), cartesianProduct(listOf("a")))
    }

    @Test
    fun testTwoElementCartesianProduct() {
        assertEquals(listOf(Pair("a", "b")), cartesianProduct(listOf("a", "b")))
    }

    @Test
    fun testThreeElementCartesianProduct() {
        val expected = listOf(
                Pair("a", "b"),
                Pair("a", "c"),
                Pair("b", "c")
        )
        assertEquals(expected, cartesianProduct(listOf("a", "b", "c")))
    }

    @Test
    fun testHammingDistance() {
        assertEquals(0, hammingDistance("aaa", "aaa"))
        assertEquals(1, hammingDistance("aaa", "aba"))
        assertEquals(3, hammingDistance("abc", "def"))
    }

}