package aoc2017.day07


import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class TowerTest {
    @Test
    fun testSampleTower() {
        val sampleTower = getSampleTower()
        assertEquals("tknk", findRoot(sampleTower))
        assertEquals(60, findWeightAdjustment(sampleTower, "tknk", 0))
    }

    @Test
    fun testParseLine() {
        assertEquals(3, parseLine("fwft (72) -> ktlj, cntj, xhth").third.size)
        assertEquals(72, parseLine("fwft (72) -> ktlj, cntj, xhth").second)
        assertTrue(parseLine("pbga (66)").third.isEmpty())
        assertEquals(66, parseLine("pbga (66)").second)
    }

    private fun getSampleTower(): Map<String, Pair<Int, List<String>?>> {
        return hashMapOf(
                "pbga" to Pair(66, emptyList()),
                "xhth" to Pair(57, emptyList()),
                "ebii" to Pair(61, emptyList()),
                "havc" to Pair(66, emptyList()),
                "ktlj" to Pair(57, emptyList()),
                "fwft" to Pair(72, listOf("ktlj", "cntj", "xhth")),
                "qoyq" to Pair(66, emptyList()),
                "padx" to Pair(45, listOf("pbga", "havc", "qoyq")),
                "tknk" to Pair(41, listOf("ugml", "padx", "fwft")),
                "jptl" to Pair(61, emptyList()),
                "ugml" to Pair(68, listOf("gyxo", "ebii", "jptl")),
                "gyxo" to Pair(61, emptyList()),
                "cntj" to Pair(57, emptyList())
        )
    }
}
