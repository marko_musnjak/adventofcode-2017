package aoc2017.day04

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource

class PassphraseTest {

    @ParameterizedTest
    @MethodSource("passphrasesWithoutAnagrams")
    fun testPassphraseValidationWithoutAnagrams(phrase: String, expected: Boolean) {
        assertEquals(expected, validatePassphrase(phrase, false))
    }

    @ParameterizedTest
    @MethodSource("passphrasesWithAnagrams")
    fun testPassphraseValidationWithAnagrams(phrase: String, expected: Boolean) {
        assertEquals(expected, validatePassphrase(phrase, true))
    }

    companion object {

        @JvmStatic
        fun passphrasesWithoutAnagrams() = listOf(
                Arguments.of("aa bb cc dd ee", true),
                Arguments.of("aa bb cc dd aa", false),
                Arguments.of("aa bb cc dd aaa", true)
        )

        @JvmStatic
        fun passphrasesWithAnagrams() = listOf(
                Arguments.of("abcde fghij", true),
                Arguments.of("abcde xyz ecdab", false),
                Arguments.of("a ab abc abd abf abj", true),
                Arguments.of("iiii oiii ooii oooi oooo", true),
                Arguments.of("oiii ioii iioi iiio", false)
        )
    }
}