package aoc2017.day05

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class InstructionTest {

    @Test
    fun testNumberOfJumps() {
        assertEquals(5, numberOfJumps(listOf(0, 3, 0, 1, -3),false))
        assertEquals(10, numberOfJumps(listOf(0, 3, 0, 1, -3), true))
    }

}
