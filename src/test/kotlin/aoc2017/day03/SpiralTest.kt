package aoc2017.day03

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource

class TestSpiral {

    @ParameterizedTest
    @MethodSource("xCoordinates")
    fun testXCoordinate(num: Int, expected: Int) {
        assertEquals(expected, getXCoordinate(num))
    }

    @ParameterizedTest
    @MethodSource("yCoordinates")
    fun testYCoordinate(num: Int, expected: Int) {
        assertEquals(expected, getYCoordinate(num))
    }

    @ParameterizedTest
    @MethodSource("distances")
    fun testDistances(num: Int, expected: Int) {
        assertEquals(expected, manhattanDistanceInSpiral(num))
    }

    companion object {

        @JvmStatic
        fun xCoordinates() = listOf(
                Arguments.of(1, 0),
                Arguments.of(2, 1),
                Arguments.of(3, 1),
                Arguments.of(4, 0),
                Arguments.of(5, -1),
                Arguments.of(6, -1),
                Arguments.of(7, -1),
                Arguments.of(8, 0),
                Arguments.of(9, 1),
                Arguments.of(10, 2)
        )

        @JvmStatic
        fun yCoordinates() = listOf(
                Arguments.of(1, 0),
                Arguments.of(2, 0),
                Arguments.of(3, 1),
                Arguments.of(4, 1),
                Arguments.of(5, 1),
                Arguments.of(6, 0),
                Arguments.of(7, -1),
                Arguments.of(8, -1),
                Arguments.of(9, -1),
                Arguments.of(10, -1),
                Arguments.of(11, 0),
                Arguments.of(12, 1),
                Arguments.of(13, 2)
        )

        @JvmStatic
        fun distances() = listOf(
                Arguments.of(1, 0),
                Arguments.of(12, 3),
                Arguments.of(23, 2),
                Arguments.of(1024, 31)
        )
    }
}