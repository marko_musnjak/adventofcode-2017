package aoc2017.day02

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TestInputs {
    private val input = """
        |5 1 9 5
        |7 5 3
        |2 4 6 8
    """.trimMargin()

    private val input2 = """
        |5 9 2 8
        |9 4 7 3
        |3 8 6 5
    """.trimMargin()

    @Test
    fun testSimpleCheckSum() {
        assertEquals(18, simpleCheckSum(input))
    }

    @Test
    fun testDivisorCheckSum() {
        assertEquals(9, divisorCheckSum(input2))
    }

}