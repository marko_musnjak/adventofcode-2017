package aoc2017.day01

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TestInputs {
    @Test
    fun offsetOne() {
        assertEquals(3, shiftedSum("1122", 1))
        assertEquals(4, shiftedSum("1111", 1))
        assertEquals(0, shiftedSum("1234", 1))
        assertEquals(9, shiftedSum("91212129", 1))
    }

    @Test
    fun offsetHalf() {
        var input = "1212"
        assertEquals(6, shiftedSum(input, input.length / 2))
        input = "1221"
        assertEquals(0, shiftedSum(input, input.length / 2))
        input = "123425"
        assertEquals(4, shiftedSum(input, input.length / 2))
        input = "123123"
        assertEquals(12, shiftedSum(input, input.length / 2))
        input = "12131415"
        assertEquals(4, shiftedSum(input, input.length / 2))
    }

}