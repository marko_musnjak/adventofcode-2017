package aoc2017.day06

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class RedistributionTest {
    @Test
    fun testRedistributionStep() {
        assertEquals(listOf(2, 4, 1, 2), redistributeOnce(listOf(0, 2, 7, 0)))
        assertEquals(listOf(3, 1, 2, 3), redistributeOnce(listOf(2, 4, 1, 2)))
        assertEquals(listOf(0, 2, 3, 4), redistributeOnce(listOf(3, 1, 2, 3)))
        assertEquals(listOf(1, 3, 4, 1), redistributeOnce(listOf(0, 2, 3, 4)))
        assertEquals(listOf(2, 4, 1, 2), redistributeOnce(listOf(1, 3, 4, 1)))
    }

    @Test
    fun testStepCount() {
        assertEquals(5, stepsBeforeRepeat(listOf(0, 2, 7, 0)).first)
        assertEquals(4, stepsBeforeRepeat(stepsBeforeRepeat(listOf(0, 2, 7, 0)).second).first)
    }
}
