package aoc2017.day08

import marko.aoc2017.day08.ConditionalInstruction
import marko.aoc2017.day08.initializeRegisters
import marko.aoc2017.day08.loadInstructions
import marko.aoc2017.day08.processInstruction
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class CpuTest {

    @Test
    fun testInitialization() {
        val instructions = getSampleInstructionSet()
        assertEquals(4, instructions.size)
        assertEquals(3, initializeRegisters(instructions).size)
    }

    @Test
    fun testRunner() {
        val instructions = getSampleInstructionSet()
        val registers = initializeRegisters(instructions)
        instructions.forEach { processInstruction(it, registers) }
        assertEquals(1, registers.map { it.value.currentValue }.max())
        assertEquals(10, registers.map { it.value.maxValue }.max())
    }

    private fun getSampleInstructionSet(): List<ConditionalInstruction> {
        val sampleInput = """
            |b inc 5 if a > 1
            |a inc 1 if b < 5
            |c dec -10 if a >= 1
            |c inc -20 if c == 10
        """.trimMargin()
        return loadInstructions(sampleInput)
    }
}